Vous pouvez lancer "main" directement.
Le jeu de la vie a été codé en première semaine par une partie du groupe,
le projet de deuxième semaine est la simulation de gas.
Par souci de cohérence et de compréhension, le projet est en anglais.
Tous les fichiers sauf ce README sont en anglais - sauf éventuellement quelques commentaires.


Sprint 1: génerer l'univers OK
    * créer l'univers OK
        la fonction crée un espace pymunk, avec une éventuelle gravité
    * ajouter des conditions aux limites OK
        constituer une "boîte" pour contenir la simulation, définir
        l'élasticité de ces parois
    * créer les particules OK
        créer la classe "particle", contenant sa masse, sa position,
        sa vitesse et sa taille (physique comme graphique)
    * ajouter les particules à l'univers OK
    * Afficher la simulation OK
        se servir de pygame pour observer la simulation

Sprint 2: simuler le gaz parfait
    * simuler le mouvement OK
        utiliser pymunk pour mettre en mouvement les particules
    * intégrer la collision avec les bords OK
    * constituer une répartition de vitesses et de position
        répartir la vitesse en fonction de la température en entrée   OK
        répartir aléatoirement les positions selon différents modèles OK
        (répartition normale, exponentielle, uniforme...)

Sprint 3: GUI OK
    * demander directement à l'utilisateur les paramètres de simulation 
        nombre de particules, distribution, taille de la boîte,
        gravité éventuelle, élasticité de la boîte

Sprint 4: complexifier le modèle
    * ajouter des objets fixes (murs...) OK
    * ajouter des collisions entre particules OK
        sortie du modèle de gaz parfait,
        première étape du gaz de Van der Waals
    * ajout des effets d'attraction et de polarisabilité - NE SERA PAS FAIT
    * compléter la GUI

Spring 5: rendre la plateforme plus ludique/pédagogique
    * ajouter des textes de description/explication
    *

Répartition:
    -Coordination et compatibilités entre fonctions, main
    -Création de la classe "particule", ajout à l'univers
    -Création de l'univers et des limites
    -Affichage de la simulation
    -Réglage des collisions

