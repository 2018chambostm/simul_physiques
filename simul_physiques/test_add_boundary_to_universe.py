from simul_physiques.add_boundary_to_universe import *
from simul_physiques.universe_generation import generate_universe
import pymunk as pk


def test_boundary_generation():
    universe_0 = generate_universe()
    universe_1 = generate_universe((100,-500))
    universe_2 = generate_universe()
    boundary_generation(universe_0, (5,5), 1, 1)
    boundary_generation(universe_1, (5,5), 1, 1)
    boundary_generation(universe_2, (5,5), 1, 1)
    assert (universe_1.gravity == (100,-500))
    assert (type(universe_1) == pk.Space)
    assert (len(universe_1.shapes) == 4)
    add_wall(universe_0,(0,0),(0,1))
    assert (type(universe_0) == pk.Space)
    assert (len(universe_0.shapes)==5)
    wall(universe_1,'Wall with one hole',0,0,(5,5),1)
    assert (type(universe_1) == pk.Space)
    assert (len(universe_1.shapes)==6)
    wall(universe_2,'Square with two holes', 0,0,(5,5),1)
    assert (len(universe_2.shapes)==8)
