import pytest # pragma: no cover
from simul_physiques.get_statistics import * # pragma: no cover
import numpy as np # pragma: no cover
from simul_physiques.universe_generation import generate_universe # pragma: no cover
import pymunk as pk # pragma: no cover


def test_get_energy():
    basic_space = generate_universe()
    mass = 1
    body_1 = pk.Body(1, moment=pymunk.moment_for_circle(mass, 0, 3))
    body_1.velocity = (1, 0)
    body_2 = pk.Body(1, moment=pymunk.moment_for_circle(mass, 0, 3))
    body_2.velocity = (3, 0)
    basic_space.add(body_1, body_2)
    assert get_energy(2, basic_space.bodies) == np.mean([1, 9])
