from simul_physiques.string_extract_to_tuple import string_to_tuple


def test_string_to_tuple():
    assert string_to_tuple("(4,3)")==(4,3)
    assert string_to_tuple("(22,67)")==(22,67)
    assert string_to_tuple("44,54,657,77")== (44,54,657,77)
