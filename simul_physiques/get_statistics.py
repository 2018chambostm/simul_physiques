import pygame
import pymunk
import numpy

'''
the objective of this program is to get some informations about each particle (with built-in functions from pymunk) and return
this information for the whole system of particles
'''


def get_energy(num_particles, particles):
    '''
    Calculates the mean kinetic energy of the particles (in arbitrary units)
    :param num_particles: number of particles in the space
    :param particles: list of pymunk bodies
    :return: the floor of the average kinetic energy
    '''
    particles_energy = []                                           # creates an array to receive the information of each particle
    for i in range(num_particles):
            particles_energy.append(particles[i].kinetic_energy)    # fills the vector with the desired information
    return int(numpy.mean(particles_energy))                        # we return an integer because it is prettier to show in the window


def get_position(num_particles, particles):
    '''
    Calculates the position of the center of mass of the gas
    :param num_particles: number of considered particles
    :param particles: list of pymunk bodies
    :return: a tuple with the average positions along x and y
    '''
    particles_position_x = [None]*num_particles                         # creates an array to receive the information of each particle
    particles_position_y = [None]*num_particles
    for i in range(num_particles):
        particles_position_x[i] = particles[i].local_to_world(particles[i].center_of_gravity)[0]  # center_of_gravity returns an vector (x,y) of the center of gravity of particle, but it it by default (0,0) because it is in the coordinates of the particle it self, but after we convert from local coodinates to local cordinates
        particles_position_y[i] = particles[i].local_to_world(particles[i].center_of_gravity)[1]
    return int(numpy.mean(particles_position_x)), int(numpy.mean(particles_position_y))
