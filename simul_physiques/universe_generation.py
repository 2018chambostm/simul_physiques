import pymunk as pk

def generate_universe(gravity_tuple=(0, 0)):
    '''
    Generate a pymunk space with several parameters (gravity,...)
    :param gravity_tuple: the gravity characteristics (a tuple)
    :return: a pymunk space
    '''
    universe = pk.Space()
    universe.gravity = gravity_tuple

    return universe
