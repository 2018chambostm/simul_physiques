import tkinter as tk
from string_extract_to_tuple import string_to_tuple
from tkinter import messagebox
from simulate_universe import simulate_universe


def display_gui():  # pragma: no cover

    def default():  # pragma: no cover
        int_variables = []      # array that will store the int values
        str_variables = []      # array that will store the str values
        tuple_variables = []    # array that will store the tuple values

        if len(nbr_particles.get()) != 0:  # checks if the entry is not null
            try:
                nbr_particles_int = int(nbr_particles.get())    # tests if the user has written an integer
            except:
                print(
                    "This value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it (: )")  # message error
                messagebox.showerror("INVALID VALUE",
                                     "Number of particules must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it ) \n\n\n\n (ᵔᴥᵔ) ")
        else:
            nbr_particles_int = 100                 # if it is null, attributes a default value
        int_variables.append(nbr_particles_int)

        int_variables.append(universe_size_x.get())    # since universe size is a scale bar, no need for checking

        int_variables.append(universe_size_y.get())

        if len(gravity_x.get()) != 0:
            try:
                gravity_x_int = int(gravity_x.get())   # checks if user has entered an integer
            except:
                print(
                    "The gravity on the x axis value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it)   ಠ‿↼")
                messagebox.showerror("INVALID VALUE",
                                     "The gravity on the x axis value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it) \n\n\n\n  ಠ‿↼")
        else:
            gravity_x_int = 0
        int_variables.append(gravity_x_int)

        if len(gravity_y.get()) != 0:
            try:
                gravity_y_int = int(gravity_y.get())
            except:
                print(
                    "The gravity on the y axis value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it) \n\n\n\n (: ")
                messagebox.showerror(
                    "The gravity on the y axis value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it (: )")
        else:
            gravity_y_int = 0
        int_variables.append(gravity_y_int)

        str_variables.append(distr_x_var.get())  # since it'a a option menu, no need for input testing
        str_variables.append(distr_y_var.get())

        if len(parameters_x.get()) != 0:                                # check for user input
            parameters_x_tuple = str(parameters_x.get())
            parameters_x_tuple = string_to_tuple(parameters_x_tuple)
        else:
            if distr_x_var.get().lower() == 'normal':                  # attributes default values for each distribution
                parameters_x_tuple = (0.5, 0.125)
            if distr_x_var.get().lower() == 'uniform':
                parameters_x_tuple = (0.0, 1.0)
            if distr_x_var.get().lower() == 'exponential':
                parameters_x_tuple = (0.125)
        tuple_variables.append(parameters_x_tuple)

        if len(parameters_y.get()) != 0:
            parameters_y_tuple = str(parameters_y.get())
            parameters_y_tuple = string_to_tuple(parameters_y_tuple)
        else:
            if distr_y_var.get().lower() == 'normal':
                parameters_y_tuple = (0.5, 0.125)
            if distr_y_var.get().lower() == 'uniform':
                parameters_y_tuple = (0.0, 1.0)
            if distr_y_var.get().lower() == 'exponential':
                parameters_y_tuple = (0.125)
        tuple_variables.append(parameters_y_tuple)

        if len(temperature.get()) != 0:     # checks if the entry is not null
            try:
                temperature_float = int(temperature.get())
            except:
                print(
                    "This value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it (: )")  # message error
                messagebox.showerror("INVALID VALUE",
                                     "Temperature must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it ) \n\n\n\n (ᵔᴥᵔ) ")
        else:
            temperature_float = 500                 # if it is null, attributes a default value
        int_variables.append(temperature_float)

        return int_variables, str_variables, tuple_variables

    def run():  # pragma: no cover
        # calls the simulation program, with all the parameters obtained by the user interface
        int_values, str_values, tuple_values = default()
        simulate_universe(int_values[0], (int_values[1], int_values[2]), gravity, (int_values[3], int_values[4]),
                          str_values[0], str_values[1], tuple_values[0], tuple_values[1], int_values[5],
                          wall_op_var.get(), hole_size.get() / 100.0) #hole size entered as an integer, but supposed to be a percentage

    def callback(event):  # pragma: no cover
        #to run the program when <Return> is pressed
        run()

    def call_parameters_g():  # pragma: no cover
        # function that opens and closes gravity entry boxes if the checkbutton is pressed
        if gravity_var.get() == 1:
            gravity_x.pack(fill='y', padx=10, pady=10)
            gravity_x_label.pack(fill='y')
            gravity_y.pack(fill='y', padx=10, pady=10)
            gravity_y_label.pack(fill='y')

        if gravity_var.get() == 0:
            gravity_x.pack_forget()
            gravity_x_label.pack_forget()
            gravity_y.pack_forget()
            gravity_y_label.pack_forget()

    def call_hole_size(event):  # pragma: no cover
        # function that opens and closes hole size scale bar depending on the wall option
        if wall_op_var.get() == 'No wall':
            hole_size.pack_forget()
            label_text.set('Wall')
        if wall_op_var.get() == 'Wall with one hole' or wall_op_var.get() == 'Wall with two holes' or wall_op_var.get() == 'Square with two holes' \
                or wall_op_var.get() == 'Two walls with one hole each' or wall_op_var.get() == 'Wall with two holes at 1/4' or wall_op_var.get() == 'Four squares':
            label_text.set('Wall \n\n\n Hole size (%)')
            hole_size.pack(fill='y', padx=2, side='top', anchor='n')
            hole_size.set(5)

    # ============ MAIN WINDOW AND FRAME ========================

    window = tk.Tk()
    window.geometry('550x730')
    window.title("Gas simulation")
    window.minsize(550, 730)

    main_frame = tk.Frame(window, bd=15, relief='sunken')
    main_frame.pack(expand=1, fill='both')

    # ============ FRAMES FOR WIDGETS ====================
    middle = tk.Frame(main_frame, )
    middle.pack(fill='both', anchor='n')

    header_frame = tk.Frame(middle)
    header_frame.pack(fill='both', expand=1, anchor='n')

    param_frame = tk.Frame(middle)
    param_frame.bind_all('<Return>', callback)
    param_frame.pack(side='top')

    par_name_frame = tk.Frame(param_frame)
    par_name_frame.pack(fill='both', side='left', anchor='w')

    param_entries_frame = tk.Frame(param_frame)
    param_entries_frame.pack(fill='both', side='right', anchor='e')

    button_frame = tk.Frame(middle)
    button_frame.pack(fill='both', anchor='s', side='bottom')

    # -------------- header ---------------------------------------------------
    header = tk.Label(header_frame, text='Gas Simulation', pady=15, font=('System', 26, 'bold'), relief='sunken', bd=15)
    header.pack(fill='both', expand=1)

    #-------------- parameters names and input spaces -------------------------
    nbr_particles = tk.Entry(param_entries_frame)
    nbr_particles.pack(fill='y', padx=10, pady=10)
    nbr_particles_label = tk.Label(par_name_frame, text='Number of particles', padx=10)
    nbr_particles_label.pack(fill='both', pady=10)

    universe_size_x = tk.Scale(param_entries_frame, from_=1, to=600, orient='horizontal', length=125)
    universe_size_x.pack(fill='y', padx=10, )
    universe_size_x.set(600)
    universe_size_x_label = tk.Label(par_name_frame, text='Length of the universe', pady=10, padx=12)
    universe_size_x_label.pack(fill='y', pady=10)

    universe_size_y = tk.Scale(param_entries_frame, from_=1, to=600, orient='horizontal', length=125)
    universe_size_y.pack(fill='y', padx=10, pady=10)
    universe_size_y.set(600)
    universe_size_y_label = tk.Label(par_name_frame, text='Height of the universe', pady=12)
    universe_size_y_label.pack(fill='y')

    temperature = tk.Entry(param_entries_frame)
    temperature.pack(fill='y', padx=10, pady=10)
    temperature_label = tk.Label(par_name_frame, text='Temperature', pady=10)
    temperature_label.pack(fill='y')

    param_distr_frame_2 = tk.Frame(param_entries_frame)
    param_distr_frame_2.pack(fill='both', side='top', anchor='n')

    distr_x_var = tk.StringVar();
    distr_x_var.set('Uniform')
    distribution_x = tk.OptionMenu(param_distr_frame_2, distr_x_var, 'Uniform', 'Normal', 'Exponential')
    distribution_x.pack(fill='y', padx=15, pady=4, side='left')
    distribution_x_label = tk.Label(par_name_frame, text='Distributions for the x and y axis', pady=10)
    distribution_x_label.pack(fill='y')

    distr_y_var = tk.StringVar();
    distr_y_var.set('Uniform')
    distribution_y = tk.OptionMenu(param_distr_frame_2, distr_y_var, 'Uniform', 'Normal', 'Exponential')
    distribution_y.pack(fill='y', padx=10, pady=4, side='left')

    param_distr_frame = tk.Frame(param_entries_frame)
    param_distr_frame.pack(fill='both', side='top', anchor='n')

    parameters_x = tk.Entry(param_distr_frame, width=13)
    parameters_x.pack(fill='y', padx=18, pady=15, side='left', anchor='n')
    parameters_y = tk.Entry(param_distr_frame, width=13)
    parameters_y.pack(fill='y', pady=15, padx=15, side='left', anchor='n')
    parameters_label = tk.Label(par_name_frame, text='Parameters for the x and y \n distributions', pady=10, padx=10)
    parameters_label.pack(fill='y')

    wall_options = tk.Frame(param_entries_frame)
    wall_options.pack(fill='both', side='top', anchor='n')

    wall_op_var = tk.StringVar();
    wall_op_var.set('No wall')
    wall_op = tk.OptionMenu(wall_options, wall_op_var, 'No wall', 'Wall with one hole', 'Wall with two holes',
                            'Square with two holes', 'Two walls with one hole each', 'Wall with two holes at 1/4',
                            'Four squares', command=call_hole_size)
    wall_op.pack(fill='y', padx=10, pady=5, side='top')

    label_text = tk.StringVar()
    label_text.set('Wall')
    wall_op_label = tk.Label(par_name_frame, textvariable=label_text, pady=10)
    wall_op_label.pack(fill='y')

    hole_size = tk.Scale(wall_options, from_=1, to=99, orient='horizontal', length=125)

    gravity_var = tk.IntVar()
    gravity = tk.Checkbutton(param_entries_frame, variable=gravity_var, pady=3, command=call_parameters_g)
    gravity.pack(fill='y', padx=10, pady=5)
    gravity_label = tk.Label(par_name_frame, text='Gravity', pady=10)
    gravity_label.pack(fill='y')

    gravity_x = tk.Entry(param_entries_frame)
    gravity_x_label = tk.Label(par_name_frame, text='Gravity value on x axis', pady=10)

    gravity_y = tk.Entry(param_entries_frame)
    gravity_y_label = tk.Label(par_name_frame, text='Gravity value on y axis', pady=10)

    #------------------ button and infos ---------------------------------------------------------------------------
    run_but = tk.Button(button_frame, bd=10, text='Run', font=('Impact', 18), command=run, activebackground='green',
                        activeforeground='black'
                        , foreground='white', background='green', relief='raised', width=17)
    run_but.pack(fill='y', padx=5, pady=20, side=tk.RIGHT)

    info = tk.Label(button_frame,
                    text="Developped by Salim HASSI, Martin CHAMBOST, Daniel FELIN, Nikos KOUGIONIS and Leonir MARCON \nin CentraleSupélec",
                    font=('TimesNewRoman', 8, 'italic'), wraplength=225)
    info.pack(fill='both', anchor='n', side='left', padx=15, pady=20)

    # ============== TOP LEVEL INFO ===================================================================================
    def display_infos():  # pragma: no cover
        infos = tk.Toplevel(window, bg='white')

        label_field = tk.Label(infos, bg='white', fg='green', text='''No field is obligatory''')
        label_field.grid(row=0, column=0)

        label_part = tk.Label(infos, bg='white', text='''Number of particles (integer) : The higher this number is, the more interactions between particles and with the boundaries there are;
                thus pressure increases with the number of particles in the universe''')
        label_part.grid(row=1, column=0)

        label_universe = tk.Label(infos, bg='white', text='''Size of the universe (two sliders) : The bigger the universe, the fewer the interactions;
                pressure decreases when the universe is bigger''')
        label_universe.grid(row=2, column=0)

        label_temperature = tk.Label(infos, bg='white', text='''Temperature (float, in Kelvin) : Temperature in the box relates to the average speed of particles with the formula 3kT=mv²
                k being Boltzmann's constant''')
        label_temperature.grid(row=3, column=0)

        label_gravity = tk.Label(infos, bg='white',
                                 text='''Gravity (two algebraic floats, arbitrary units) : influences the global motion of particles on a given direction''')
        label_gravity.grid(row=4, column=0)

        label_distribution = tk.Label(infos, bg='white', text='''Distributions : 
        - Uniform (two input parameters  : extreme values of the distribution, between 0 and 1)
        - Normal (two input parameters : mean and positive standard deviation)
        - Exponential (one input parameter : strictly positive mean)''')
        label_distribution.grid(row=5, column=0)

        label_origin = tk.Label(infos, bg='white', fg='green',
                                text='''The origin of the coordinate system is set in the bottom left corner''')
        label_origin.grid(row=6, column=0)

    infos_button = tk.Button(button_frame, text=chr(9432), command=display_infos, bd=10, font=('Impact', 20, 'bold'),
                             activebackground='lightblue', activeforeground='black'
                             , foreground='white', background='blue', relief='raised', width=3)
    infos_button.pack(fill='y', pady=20, side='left')

    # ============== MAINLOOP FUNCTION ================================================
    window.mainloop()


if __name__ == "__main__":
    display_gui()
