import sys
from pygame.locals import *
import pymunk
import pygame
import pymunk.pygame_util
from universe_generation import *
from add_boundary_to_universe import *
from particle import *
from get_statistics import *


def simulate_universe(num_of_particles:int, boundary_size:tuple, gravity:bool = False, gravity_value:tuple =(0,0), distribution_x:str='normal', distribution_y:str='normal',
         parameter_x=(0.5,0.125), parameter_y=(0.5,0.125), temperature:float=300, wall_op='No wall', hole_size=0): # pragma: no cover
    pygame.init()                                           # initialization of the game and display module
    screen_width = 610
    screen_height = 670
    screen = pygame.display.set_mode((screen_width, screen_height))      # setting of the display resolution
    clock = pygame.time.Clock()                                          # creation of an object to monitor time

    space = generate_universe(gravity_value)                                      # creation of universe
    x0, y0 = boundary_generation(space, boundary_size, screen_width, screen_height)    # Added boundaries to the universe
    particles, gas = add_particles(space, num_of_particles, temperature, x0, y0, boundary_size, distribution_x, distribution_y, parameter_x, parameter_y)
    wall(space, wall_op,x0, y0, boundary_size, hole_size)
    energy_avarge = get_energy(num_of_particles, particles)
    position_avarage = get_position(num_of_particles, particles)
    print(energy_avarge,position_avarage)
    draw_options = pymunk.pygame_util.DrawOptions(screen)

    while True:

        energy_avarge = get_energy(num_of_particles, particles)
        position_avarage = get_position(num_of_particles, particles)

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.display.quit()
                pygame.quit()

            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                pygame.display.quit()
                pygame.quit()

        space.step(1/500)                          # setting the time step of the for which the universe is actualized

        screen.fill((255, 255, 255))                  # fills the screen with one color (white)
        space.debug_draw(draw_options)


        position_font = pygame.font.SysFont('Calibri',16)                                               #defines the font and size of text
        position_text = position_font.render("The gravity center of this system is: " +str(position_avarage),1,(255,0,0)) #text to be printed

        energy_font = pygame.font.SysFont('Calibri',16)
        energy_text = position_font.render("The avarage energy of the particules is: " +str(energy_avarge)+ "(arbitrary unit)",1,(255,0,0))

        screen.blit(position_text, (40,10))             
        screen.blit(energy_text, (40,40))

        pygame.display.flip()                       # update the full display to the screen
        clock.tick(500)                             # sets the next tick to 50 ms
