def string_to_tuple(origin_str: str):
    """
    This function will scrap an input string representing a 2 elements tupple,
    the idea is to separate the string, delete the brackets, and output a tuple
    :param origin_str: a string representing a 2 elements tuple
    :return: the corresponding tuple
    """
    used_list = list(origin_str)
    while "(" in used_list:
        used_list.remove("(")
    while ")" in used_list:
        used_list.remove(")")
    if "," not in used_list:
        content_str = ""
        for digit in used_list:
            content_str += digit
        return float(content_str)
    coma_number = used_list.count(',')  # the number of comas
    final_list = []
    for i in range(coma_number):
        coma_index = used_list.index(",")  # we localise the separator
        element_str = ""
        for digit in used_list[:coma_index]:
            element_str += digit  # we add the digits before the coma to a string
        used_list = used_list[coma_index+1:]
        final_list.append(float(element_str))
    element_str = ""
    for digit in used_list:
        element_str += digit  # we add the last digits of the string
    final_list.append(float(element_str))

    return tuple(final_list)
