from simul_physiques.particle import *
from simul_physiques.universe_generation import *
from simul_physiques.add_boundary_to_universe import *


def test_particle():
    screen_width = 800
    screen_height = 500
    boundary_size = (700, 400)
    num_of_particles = 1000
    temperature = 300
    space = generate_universe((0, 0))                                      # creation of universe
    x0, y0 = boundary_generation(space, boundary_size, screen_width, screen_height)
    gas = add_particles(space, num_of_particles, temperature, x0, y0, boundary_size, 'Uniform', 'Uniform', (0, 1), (0, 1))[1]
    for i in range(num_of_particles):
        assert (gas[i] is not None)
