import pymunk as pk
import pygame.color


def boundary_generation(universe:pk.Space, size:tuple, screen_width:float, screen_height:float, elasticity:float=1, thickness:float=1):
    '''
    Given a pymunk space, a size and a value of elasticity returns the universe with a rectangle
    to delimit a finite universe and the elasticity of its walls
    :param universe: the universe to modify
    :param size: length and height of the walls
    :param elasticity: elasticity of the walls (tuple between 0.0 and 1.9)
    :param thickness: thickness of the walls
    :return None
    '''

    if not(0.0<=elasticity<=1.9):
        raise ValueError('Elasticity must be in [0.0,1.9]')
    elif thickness<0:
        raise ValueError('Thickness must be a positive float')

    length,height = size
    x0 = (screen_width - length)/2
    y0 = (screen_height - 60 -  height)/2
    segment_down=pk.Segment(universe.static_body, (x0,y0),(length+x0,y0),thickness)
    segment_right=pk.Segment(universe.static_body, (length+x0,y0),(length+x0,height+y0),thickness)
    segment_up=pk.Segment(universe.static_body, (x0,height+y0),(length+x0,height+y0),thickness)
    segment_left=pk.Segment(universe.static_body, (x0,y0),(x0,height+y0),thickness)

    segments=[segment_down,segment_left,segment_right,segment_up]

    for segment in segments:
        segment.color = pygame.color.THECOLORS["black"]
        segment.elasticity = elasticity
        universe.add(segment)
    return x0, y0

def add_wall(universe:pk.Space,  coordinates_1: tuple, coordinates_2: tuple, elasticity: float=1, thickness:float=1):
    '''
    Given a pymunk space, places a wall at given coordinates with given properties(elasticity and thickness)
    :param universe: the universe to modify
    :param coordinates_1: coordinates of the first point
    :param coordinates_1: coordinates of the second point
    :param elasticity: elasticity of the walls (tuple between 0.0 and 1.9)
    :param thickness: thickness of the walls
    :return None
    '''
    if not(0.0<=elasticity<=1.9):
        raise ValueError('Elasticity must be in [0.0,1.9]')
    wall=pk.Segment(universe.static_body, coordinates_1,coordinates_2,thickness)
    wall.color = pygame.color.THECOLORS["black"]
    wall.elasticity = elasticity
    universe.add(wall)

def wall(universe, wall_op, x0, y0, boundary_size, hole_size):

    if wall_op == 'Wall with one hole':                                                    # creates a wall with just one hole
        coord_1 = (x0+boundary_size[0]/2.0 , y0)                                           # coordinates of the first half of wall
        coord_2 = (x0+boundary_size[0]/2.0 , y0+boundary_size[1]/2.0*(1 - hole_size))      # final coordinates of the first half of wall - as the hole_size is a percentage, the first half will go untill the middle minus the porcentage of the hole
        add_wall(universe, coord_1, coord_2)                                               # add the first half to the universe
        coord_1 = (x0+boundary_size[0]/2.0 , y0+boundary_size[1]/2.0*(1 + hole_size))      # the beginning of the second half
        coord_2 = (x0+boundary_size[0]/2.0 , y0+boundary_size[1])                          # end point of the first half
        add_wall(universe, coord_1, coord_2)

    if wall_op == 'Wall with two holes':                                                   # same thing of the Wall with one hole, but now there are three walls
        coord_1 = (x0 + boundary_size[0]/2.0, y0)
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/3*(1 - hole_size))
        add_wall(universe, coord_1, coord_2)                                               # addition of first wall
        coord_1 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/3*(1 + hole_size))
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/3*(1 + hole_size) + boundary_size[1]*(1 - hole_size)/3.0)
        add_wall(universe, coord_1, coord_2)                                               # addition of second wall
        coord_1 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/3*(1 + hole_size) + boundary_size[1]*(1 + hole_size)/3.0)
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1])
        add_wall(universe, coord_1, coord_2)                                               # addition of third wall

    if wall_op == 'Square with two holes':
        '''now we are creating a square of a quarter of the total area, with one hole in each face, both with same sizes
           for this, we are going to create two walls in each face, 4 walls in total 
        '''
        #vertical wall
        coord_1 = (x0 + boundary_size[0]/2.0, y0)
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/4.0*(1 - hole_size))
        add_wall(universe, coord_1, coord_2)                                                # we create the first wall of the vertical side of our square
        coord_1 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/4.0*(1 + hole_size))
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/2.0)
        add_wall(universe, coord_1, coord_2)                                                # add the second wall of the vertical side of our square
        #horizontal wall
        coord_1 = (x0, y0 + boundary_size[1]/2.0)
        coord_2 = (x0 + boundary_size[0]/4.0*(1 - hole_size), y0 + boundary_size[1]/2.0)
        add_wall(universe, coord_1, coord_2)                                                # add the first wall of the horizontal side of our square
        coord_1 = (x0 + boundary_size[0]/4.0*(1 + hole_size), y0 + boundary_size[1]/2.0)
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/2.0)
        add_wall(universe, coord_1, coord_2)                                                # add the second wall of the horizontal side of our square

    if wall_op == 'Two walls with one hole each':
        #wall 1
        coord_1 = (x0+boundary_size[0]*1.0/3.0 , y0)                                        # coordinates of the first half of first wall
        coord_2 = (x0+boundary_size[0]*1.0/3.0 , y0+boundary_size[1]/2.0*(1 - hole_size))   # final coordinates of the first half of first wall - as the hole_size is a percentage, the first half will go untill the middle minus the porcentage of the hole
        add_wall(universe, coord_1, coord_2)                                                # add the first half to the universe
        coord_1 = (x0+boundary_size[0]*1.0/3.0 , y0+boundary_size[1]/2.0*(1 + hole_size))   # the beginning of the second half
        coord_2 = (x0+boundary_size[0]*1.0/3.0 , y0+boundary_size[1])                       # end point of the first half
        add_wall(universe, coord_1, coord_2)

        #wall 2
        coord_1 = (x0+boundary_size[0]*2.0/3.0 , y0)                                        # coordinates of the first half of first wall
        coord_2 = (x0+boundary_size[0]*2.0/3.0 , y0+boundary_size[1]/2.0*(1 - hole_size))   # final coordinates of the first half of first wall - as the hole_size is a percentage, the first half will go untill the middle minus the porcentage of the hole
        add_wall(universe, coord_1, coord_2)                                                # add the first half to the universe
        coord_1 = (x0+boundary_size[0]*2.0/3.0 , y0+boundary_size[1]/2.0*(1 + hole_size))   # the beginning of the second half
        coord_2 = (x0+boundary_size[0]*2.0/3.0 , y0+boundary_size[1])                       # end point of the first half
        add_wall(universe, coord_1, coord_2)

    if wall_op == 'Four squares':
        #vertical bottom wall
        coord_1 = (x0 + boundary_size[0]/2.0, y0)
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/4.0*(1 - hole_size))
        add_wall(universe, coord_1, coord_2)                                                # we create the first wall of the vertical side of our square
        coord_1 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/4.0*(1 + hole_size))
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/2.0)
        add_wall(universe, coord_1, coord_2)                                                # add the second wall of the vertical side of our square
        #horizontal left wall
        coord_1 = (x0, y0 + boundary_size[1]/2.0)
        coord_2 = (x0 + boundary_size[0]/4.0*(1 - hole_size), y0 + boundary_size[1]/2.0)
        add_wall(universe, coord_1, coord_2)                                                # add the first wall of the horizontal side of our square
        coord_1 = (x0 + boundary_size[0]/4.0*(1 + hole_size), y0 + boundary_size[1]/2.0)
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/2.0)
        add_wall(universe, coord_1, coord_2)
        #vertical top wall
        coord_1 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/2.0)
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/2.0 + boundary_size[1]/4.0*(1 - hole_size))
        add_wall(universe, coord_1, coord_2)                                                # we create the first wall of the vertical side of our square
        coord_1 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/2.0 + boundary_size[1]/4.0*(1 + hole_size))
        coord_2 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1])
        add_wall(universe, coord_1, coord_2)                                                # add the second wall of the vertical side of our square
        #horizontal right wall
        coord_1 = (x0 + boundary_size[0]/2.0, y0 + boundary_size[1]/2.0)
        coord_2 = (x0 + boundary_size[0]/2.0 + boundary_size[0]/4.0*(1 - hole_size), y0 + boundary_size[1]/2.0)
        add_wall(universe, coord_1, coord_2)                                                # add the first wall of the horizontal side of our square
        coord_1 = (x0 + boundary_size[0]/2.0 + boundary_size[0]/4.0*(1 + hole_size), y0 + boundary_size[1]/2.0)
        coord_2 = (x0 + boundary_size[0]/2.0 + boundary_size[0]/2.0, y0 + boundary_size[1]/2.0)
        add_wall(universe, coord_1, coord_2)

    if wall_op == 'Wall with two holes at 1/4':                                                   # same thing of the Wall with one hole, but now there are three walls
        coord_1 = (x0 + boundary_size[0]/4.0, y0)
        coord_2 = (x0 + boundary_size[0]/4.0, y0 + boundary_size[1]/3*(1 - hole_size))
        add_wall(universe, coord_1, coord_2)                                               # addition of first wall
        coord_1 = (x0 + boundary_size[0]/4.0, y0 + boundary_size[1]/3*(1 + hole_size))
        coord_2 = (x0 + boundary_size[0]/4.0, y0 + boundary_size[1]/3*(1 + hole_size) + boundary_size[1]*(1 - hole_size)/3.0)
        add_wall(universe, coord_1, coord_2)                                               # addition of second wall
        coord_1 = (x0 + boundary_size[0]/4.0, y0 + boundary_size[1]/3*(1 + hole_size) + boundary_size[1]*(1 + hole_size)/3.0)
        coord_2 = (x0 + boundary_size[0]/4.0, y0 + boundary_size[1])
        add_wall(universe, coord_1, coord_2)                                               # addition of third wall

