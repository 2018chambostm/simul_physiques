import random
import numpy as np
from simul_physiques.position_distribution import *
import pymunk


class Particle:  # pragma: no cover
    """Holds the Position, Velocity, Mass, Radius of a given particle """

    def __init__(self, mass, radius, temperature, distribution_x="Uniform", distribution_y="Uniform", parameter_x=(0,1), parameter_y=(0,1)):
        self.mass = mass
        self.radius = radius
        self.speed_x = None
        self.speed_y = None
        self.pos_x, self.pos_y = position_distribution(distribution_x, distribution_y, parameter_x, parameter_y)
        self.speed_x = np.sqrt((300*temperature)/self.mass)*random.gauss(0, 0.3)
        self.speed_y = np.sqrt((300*temperature)/self.mass)*random.gauss(0, 0.3)


def add_particles(space, num_of_particles, temperature, x0, y0, boundary_size, distribution_x, distribution_y, parameter_x, parameter_y, mass=1, radius=3):
    '''
    Adds a given number of particles to the space, following a pre-set distribution
    :param space: pymunk space, the working space
    :param num_of_particles: integer, the number of particles to add to space
    :param temperature: temperature in kelvin, to define the speed of the particles
    :param x0: integer, coordinate of the corner of the box on the x axis
    :param y0: same on y axis
    :param boundary_size: tuple, the size of the box
    :param distribution_x: string, name of the preset distribution on x axis (to be used in psition_distribution
    :param distribution_y: same on y axis
    :param parameter_x: tuple, set of parameters for the x distribution (depends on the distribution mode)
    :param parameter_y: same for y distribution
    :param mass: mass of the generated particles (default to 1)
    :param radius: radius of the particles (default to 3)
    :return: tuple : the list of the pymunk bodies added to space, and the list of the particles added
    '''
    gas = [None]*num_of_particles  # initialize the returned lists
    body = [None]*num_of_particles
    for i in range(num_of_particles):  # each particle is generated and added individually
        gas[i] = Particle(mass=mass, radius=radius, temperature=temperature, distribution_x=distribution_x,
                          distribution_y=distribution_y,parameter_x=parameter_x, parameter_y=parameter_y )
        moment = pymunk.moment_for_circle(mass, 0, radius)  # the particles do not rotate
        body[i] = pymunk.Body(mass, moment)
        body[i].position = gas[i].pos_x * boundary_size[0] + x0, gas[i].pos_y * boundary_size[1] + y0
        # adapts the random position of gas (between 0 and 1) to the size of the universe)
        body[i].velocity = gas[i].speed_x, gas[i].speed_y
        # attributes a speed to the bodies, the speed is generated randomly from the temperature, under a normal law
        shape = pymunk.Circle(body[i], radius)
        # the particles are modelled as circles
        shape.elasticity = 1
        # this elasticity induces a perfect bounce on the boundaries and between particles
        space.add(body[i], shape)

    return body, gas
