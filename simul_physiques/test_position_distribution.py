from simul_physiques.position_distribution import *  # pragma: no cover

def test_position_distribution():

    for i in range(1000):
        x_position, y_position = position_distribution("normal", "normal", (0.5,0.5),(0.5, 0.5))
        print(x_position,y_position)
        assert (y_position <= 1.0 and x_position <= 1.0 and y_position >= 0.0 and x_position >= 0.0)

    for i in range(1000):
        x_position, y_position = position_distribution("exponential", "exponential", 0.125, 0.125)
        assert (y_position <= 1.0 and x_position <= 1.0 and y_position >= 0.0 and x_position >= 0.0)

    for i in range(1000):
        x_position, y_position = position_distribution("uniform","uniform", (0,1), (0,1))
        assert (y_position <= 1.0 and x_position <= 1.0 and y_position >= 0.0 and x_position >= 0.0)
