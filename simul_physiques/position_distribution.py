import random

def position_distribution(distribution_x , distribution_y, parametre_x, parametre_y ):

    if distribution_x.lower() == "uniform":                       #check what was the entry given
        begin_point = parametre_x[0]
        end_point = parametre_x[1]
        x_position =  random.uniform(begin_point,end_point)                          #calculate the normalized value of position

    if distribution_x.lower() == "normal":
        avarage = parametre_x[0]
        standart_deviation = parametre_x[1]

        x_position = random.gauss(avarage,standart_deviation)
        if x_position > 1.0:                                      #check if the values are valid, and if not, set it to the most proximate valid number
            x_position = 1.0
        if x_position < 0.0:
            x_position = 0.0

    if distribution_x.lower() == "exponential":

        exponential_factor = parametre_x
        x_position = random.expovariate(exponential_factor)
        if x_position > 1.0:
            x_position = 1.0


    if distribution_y.lower() == "uniform":                       #check what was the entry given
        begin_point = parametre_y[0]
        end_point = parametre_y[1]
        y_position =  random.uniform(begin_point,end_point)                          #calculate the normalized value of position

    if distribution_y.lower() == "normal":
        avarage = parametre_y[0]
        standart_deviation = parametre_y[1]

        y_position = random.gauss(avarage, standart_deviation)
        if y_position > 1.0:                                      #check if the values are valid, and if not, set it to the most proximate valid number
            y_position = 1.0
        if y_position < 0.0:
            y_position = 0.0

    if distribution_y.lower() == "exponential":
        exponential_factor = parametre_y
        y_position = random.expovariate(1.0/exponential_factor)
        if y_position > 1.0:
            y_position = 1.0


    return x_position, y_position                              #return one value for each coordinate
