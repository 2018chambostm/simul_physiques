from simul_physiques.universe_generation import generate_universe
import pymunk as pk


def test_universe_generation():
    assert (type(generate_universe()) == pk.Space)
    assert (generate_universe((50,50)).gravity == (50,50))
